<?php
//header("Access-Control-Allow-Origin: https://luisvardez.000webhostapp.com");

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';
require '../src/config/db.php';

$app = new \Slim\App;

$c = $app->getContainer();
$c['notAllowedHandler'] = function ($c) {
    return function ($request, $response, $methods) use ($c) {
        return $c['response']
            ->withStatus(405)
            ->withHeader('Allow', implode(', ', $methods))
            ->withHeader('Content-type', 'text/html')
            ->write('Method must be one of: ' . implode(', ', $methods));
    };
};

$app->get('/hello/{name}', function (Request $request, Response $response) {
    $name = $request->getAttribute('name');
    $response->getBody()->write("Hello, $name");

    return $response;
});

//$method = $_SERVER['REQUEST_METHOD'];

//echo $method;


$app->group('/api',function() use($app){

	$app->group('/users',function() use($app){

			$app->get('',function(Request $request,Response $response){
				$sql = "SELECT * FROM user";

				try{
					//get db object
					$db = new db();
					//connect
					$db = $db->connect();

					$stmt = $db->query($sql);
					$customers = $stmt->fetchAll(PDO::FETCH_OBJ);
					$db = null;

					echo json_encode($customers);
				}catch(PDOException $e){
					echo '{"error":{"text":'.$e->getMessage().'}}';
				}
			});

			$app->post('/add',function(Request $request,Response $response){
				$age = $request->getParam('age');
				$genre = $request->getParam('genre');
				$latitud = $request->getParam('latitud');
				$longitud = $request->getParam('longitud');

				$sql = "INSERT INTO user(age,genre,latitud,longitud) VALUES
				(:age,:genre,:latitud,:longitud)";

				try{
					//get db object
					$db = new db();
					//connect
					$db = $db->connect();

					$stmt = $db->prepare($sql);

					$stmt->bindParam(':age',$age);
					$stmt->bindParam(':genre',$genre);
					$stmt->bindParam(':latitud',$latitud);
					$stmt->bindParam(':longitud',$longitud);

					$stmt->execute();

					//echo '{"notice":{"text":"customer Added}}';
					echo '{"notice":{"text":"customer Added"}}';

				}catch(PDOException $e){
					echo '{"error":{"text":'.$e->getMessage().'}}';
				}
			});
	});

	$app->group('/collections',function() use($app){

			$app->get('',function(Request $request,Response $response){
				$sql = "SELECT * FROM collection";

				try{
					//get db object
					$db = new db();
					//connect
					$db = $db->connect();

					$stmt = $db->query($sql);
					$collections = $stmt->fetchAll(PDO::FETCH_OBJ);
					$db = null;

					echo json_encode($collections);
					
				}catch(PDOException $e){
					echo '{"error":{"text":'.$e->getMessage().'}}';
				}
			});

			$app->get('/{id}',function(Request $request,Response $response){
				$id = $request->GetAttribute('id');

				$sql = "SELECT * FROM customers WHERE id = $id";

				try{
					//get db object
					$db = new db();
					//connect
					$db = $db->connect();

					$stmt = $db->query($sql);
					$customers = $stmt->fetchAll(PDO::FETCH_OBJ);
					$db = null;

					echo json_encode($customers);
				}catch(PDOException $e){
					echo '{"error":{"text":'.$e->getMessage().'}}';
				}
			});

			$app->post('/add',function(Request $request,Response $response){
				$name = $request->getParam('name');
				$coverUrl = $request->getParam('coverUrl');


				$sql = "INSERT INTO collection(name,coverUrl) VALUES
				(:name,:coverUrl)";

				try{
					//get db object
					$db = new db();
					//connect
					$db = $db->connect();

					$stmt = $db->prepare($sql);

					$stmt->bindParam(':name',$name);
					$stmt->bindParam(':coverUrl',$coverUrl);

					$stmt->execute();

					echo '{"notice":{"text":"collection Added"}}';

				}catch(PDOException $e){
					echo '{"error":{"text":'.$e->getMessage().'}}';
				}
			});
	});

	$app->group('/notebooks',function() use($app){

		//obtencion de lista de libretas
		$app->get('',function(Request $request,Response $response){
			$sql = "SELECT * FROM notebook";

			try{
				//get db object
				$db = new db();
				//connect
				$db = $db->connect();

				$stmt = $db->query($sql);
				$collections = $stmt->fetchAll(PDO::FETCH_OBJ);
				$db = null;

				echo json_encode($collections);
				
			}catch(PDOException $e){
				echo '{"error":{"text":'.$e->getMessage().'}}';
			}
		});

		//agregar una libreta a la base de datos
		$app->post('/add',function(Request $request,Response $response){
			$name = $request->getParam('name');
			$coverSource = $request->getParam('coverSource');
			$listCoverSource = $request->getParam('listCoverSource');
			$collection = $request->getParam('collection');
			$descripcion = $request->getParam('descripcion');

			$sql = "INSERT INTO notebook(name,coverSource,listCoverSource,collection,descripcion) VALUES
			(:name,:coverSource,:listCoverSource,:collection,:descripcion)";

			try{
				//get db object
				$db = new db();
				//connect
				$db = $db->connect();

				$stmt = $db->prepare($sql);

				$stmt->bindParam(':name',$name);
				$stmt->bindParam(':coverSource',$coverSource);
				$stmt->bindParam(':listCoverSource',$listCoverSource);
				$stmt->bindParam(':collection',$collection);
				$stmt->bindParam(':descripcion',$descripcion);

				$stmt->execute();

				echo '{"notice":{"text":"notebook Added"}}';

			}catch(PDOException $e){
				echo '{"error":{"text":'.$e->getMessage().'}}';
			}
		});

		//lista de libretas de acuerdo a la coleccion seleccionada
		$app->get('/{idCollection}',function(Request $request,Response $response){
			$id = $request->GetAttribute('idCollection');

			$sql = "SELECT * FROM notebook WHERE collection = $id";

			try{
				//get db object
				$db = new db();
				//connect
				$db = $db->connect();

				$stmt = $db->query($sql);
				$notebooks = $stmt->fetchAll(PDO::FETCH_OBJ);
				$db = null;

				echo json_encode($notebooks);
			}catch(PDOException $e){
				echo '{"error":{"text":'.$e->getMessage().'}}';
			}
		});

		//onbtención  de datos de una libreta en especifico
		$app->get('/search/{idNotebook}',function(Request $request,Response $response){
			$id = $request->GetAttribute('idNotebook');

			$sql = "SELECT * FROM notebook WHERE id = $id";

			try{
				//get db object
				$db = new db();
				//connect
				$db = $db->connect();

				$stmt = $db->query($sql);
				$notebooks = $stmt->fetchAll(PDO::FETCH_OBJ);
				$db = null;

				echo json_encode($notebooks);
			}catch(PDOException $e){
				echo '{"error":{"text":'.$e->getMessage().'}}';
			}
		});

		//actualiza datos de una libreta en especifi
		$app->put('/update/{idNote}',function(Request $request,Response $response){
			$id = $request->getAttribute('idNote');
			//obtención del parametro que viene desde la liga

			$like = $request->getParam("like");
			//obtención de parametro que se envia desde data

			$sql = "UPDATE notebook SET dislike = :like WHERE id = " . $id;

			try{
				//get db object
				$db = new db();
				//connect
				$db = $db->connect();

				$stmt = $db->prepare($sql);

				$stmt->bindParam(":like",$like);

				$stmt->execute();

				echo '{"notice":{"text":"notebook Updated"}}';

			}catch(PDOException $e){
				echo '{"error":{"text":'.$e->getMessage().'}}';
			}
		});

		//borra un registro de libretas
		$app->delete('/delete/{idNote}',function(Request $request,Response $response){
			$id = $request->getAttribute('idNote');

			$sql = "DELETE FROM notebook WHERE id = " . $id;

			try{
				$db = new db();
				$db = $db->connect();

				$stmt = $db->prepare($sql);
				$stmt->execute();

				echo '{"notice":{"text":"notebook Erased"}}';

			}catch(PDOException $e){
				echo '{"error":{"text":'.$e->getMessage().'}}';
			}
		});
	});
});

$app->run();