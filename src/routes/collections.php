<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App;

//Get all customers

$app->get('/api/collections',function(Request $request,Response $response){
	$sql = "SELECT * FROM collection";

	try{
		//get db object
		$db = new db();
		//connect
		$db = $db->connect();

		$stmt = $db->query($sql);
		$collections = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;

		echo json_encode($collections);
	}catch(PDOException $e){
		echo '{"error":{"text":'.$e->getMessage().'}}';
	}
});

//get single customer
$app->get('/api/collections/{id}',function(Request $request,Response $response){
	$id = $request->GetAttribute('id');

	$sql = "SELECT * FROM customers WHERE id = $id";

	try{
		//get db object
		$db = new db();
		//connect
		$db = $db->connect();

		$stmt = $db->query($sql);
		$customers = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;

		echo json_encode($customers);
	}catch(PDOException $e){
		echo '{"error":{"text":'.$e->getMessage().'}}';
	}
});

//add Customer
$app->post('/api/collections/add',function(Request $request,Response $response){
	$name = $request->getParam('name');
	$coverUrl = $request->getParam('coverUrl');


	$sql = "INSERT INTO collection(name,coverUrl) VALUES
	(:name,:coverUrl)";

	try{
		//get db object
		$db = new db();
		//connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':name',$name);
		$stmt->bindParam(':coverUrl',$coverUrl);

		$stmt->execute();

		echo '{"notice":{"text":"collection Added"}}';

	}catch(PDOException $e){
		echo '{"error":{"text":'.$e->getMessage().'}}';
	}
});

//update Customer
$app->put('/api/collections/update/id',function(Request $request,Response $response){
	$id = $request->getAttribute('id');
	$first_name = $request->getAttribute('first_name');
	$last_name = $request->getAttribute('last_name');
	$phone = $request->getAttribute('phone');
	$email = $request->getAttribute('email');
	$address = $request->getAttribute('address');
	$city = $request->getAttribute('city');
	$state = $request->getAttribute('state');

	$sql = "UPDATE customers SET
					first_name 	= :first_name,
					last_name 	= :last_name,
					phone		= :phone,
					email		= :email,
					address 	= :address,
					city		= :city,
					state		= :state
			WHERE id = $id";

	try{
		//get db object
		$db = new db();
		//connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':first_name',$first_name);
		$stmt->bindParam(':last_name',$last_name);
		$stmt->bindParam(':phone',$phone);
		$stmt->bindParam(':email',$email);
		$stmt->bindParam(':address',$address);
		$stmt->bindParam(':city',$city);
		$stmt->bindParam(':state',$state);

		$stmt->execute();

		echo '{"notice":{"text":"customer update"}}';

	}catch(PDOException $e){
		echo '{"error":{"text":'.$e->getMessage().'}}';
	}
});


//delete customer
$app->delete('/api/collections/delete/{id}',function(Request $request,Response $response){
	$id = $request->GetAttribute('id');

	$sql = "DELETE FROM customers WHERE id = $id";

	try{
		//get db object
		$db = new db();
		//connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);
		$stmt->execute();
		$db = null;

		echo '{"notice":{"text":"customer deleted"}}';
	}catch(PDOException $e){
		echo '{"error":{"text":'.$e->getMessage().'}}';
	}
});