<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App;

//Get all customers

$app->get('/api/users',function(Request $request,Response $response){
	$sql = "SELECT * FROM user";

	try{
		//get db object
		$db = new db();
		//connect
		$db = $db->connect();

		$stmt = $db->query($sql);
		$customers = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;

		echo json_encode($customers);
	}catch(PDOException $e){
		echo '{"error":{"text":'.$e->getMessage().'}}';
	}
});

//get single customer
$app->get('/api/customers/{id}',function(Request $request,Response $response){
	$id = $request->GetAttribute('id');

	$sql = "SELECT * FROM customers WHERE id = $id";

	try{
		//get db object
		$db = new db();
		//connect
		$db = $db->connect();

		$stmt = $db->query($sql);
		$customers = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;

		echo json_encode($customers);
	}catch(PDOException $e){
		echo '{"error":{"text":'.$e->getMessage().'}}';
	}
});

//add Customer
$app->post('/api/users/add',function(Request $request,Response $response){
	$age = $request->getParam('age');
	$genre = $request->getParam('genre');
	$latitud = $request->getParam('latitud');
	$longitud = $request->getParam('longitud');

	$sql = "INSERT INTO user(age,genre,latitud,longitud) VALUES
	(:age,:genre,:latitud,:longitud)";

	try{
		//get db object
		$db = new db();
		//connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':age',$age);
		$stmt->bindParam(':genre',$genre);
		$stmt->bindParam(':latitud',$latitud);
		$stmt->bindParam(':longitud',$longitud);

		$stmt->execute();

		//echo '{"notice":{"text":"customer Added}}';
		echo '{"notice":{"text":"customer Added"}}';

	}catch(PDOException $e){
		echo '{"error":{"text":'.$e->getMessage().'}}';
	}
});

//update Customer
$app->put('/api/customers/update/id',function(Request $request,Response $response){
	$id = $request->getAttribute('id');
	$first_name = $request->getAttribute('first_name');
	$last_name = $request->getAttribute('last_name');
	$phone = $request->getAttribute('phone');
	$email = $request->getAttribute('email');
	$address = $request->getAttribute('address');
	$city = $request->getAttribute('city');
	$state = $request->getAttribute('state');

	$sql = "UPDATE customers SET
					first_name 	= :first_name,
					last_name 	= :last_name,
					phone		= :phone,
					email		= :email,
					address 	= :address,
					city		= :city,
					state		= :state
			WHERE id = $id";

	try{
		//get db object
		$db = new db();
		//connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':first_name',$first_name);
		$stmt->bindParam(':last_name',$last_name);
		$stmt->bindParam(':phone',$phone);
		$stmt->bindParam(':email',$email);
		$stmt->bindParam(':address',$address);
		$stmt->bindParam(':city',$city);
		$stmt->bindParam(':state',$state);

		$stmt->execute();

		echo '{"notice":{"text":"customer update"}}';

	}catch(PDOException $e){
		echo '{"error":{"text":'.$e->getMessage().'}}';
	}
});


//delete customer
$app->delete('/api/customers/delete/{id}',function(Request $request,Response $response){
	$id = $request->GetAttribute('id');

	$sql = "DELETE FROM customers WHERE id = $id";

	try{
		//get db object
		$db = new db();
		//connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);
		$stmt->execute();
		$db = null;

		echo '{"notice":{"text":"customer deleted"}}';
	}catch(PDOException $e){
		echo '{"error":{"text":'.$e->getMessage().'}}';
	}
});